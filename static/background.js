/* global chrome */

chrome.runtime.onInstalled.addListener(function() {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([
      {
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { hostEquals: "localhost" }
          })
        ],
        actions: [new chrome.declarativeContent.ShowPageAction()]
      }
    ])
  })
})

let tabId = null
chrome.runtime.onConnect.addListener(function(port) {
  if (port.name === "settingsRequest") {
    // REQUEST
    port.onMessage.addListener(function(message, sender) {
      chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
        tabId = tabId || tabs[0].id
        chrome.tabs.sendMessage(tabId, { data: message }, function(response) {
          port.postMessage({ response: response })
        })
      })
    })
  } else if (port.name === "settingsPost") {
    // POST
    port.onMessage.addListener(function(message, sender) {
      chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
        tabId = tabId || tabs[0].id
        chrome.tabs.sendMessage(tabId, { data: message }, function(response) {
          port.postMessage({ response: response })
        })
      })
    })
  }
})
