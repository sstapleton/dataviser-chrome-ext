import * as ReactDOM from "react-dom"
import App from "../app/app"
import "./index.css"

const rootId = document.getElementById("extension-root")

if (rootId) ReactDOM.render(<App />, rootId)
