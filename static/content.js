/* global chrome */

chrome.runtime.onMessage.addListener(function(message, sender, senderResponse) {
  const { data } = message
  const key = Object.keys(data)[0]
  const value = Object.values(data)[0]

  if (key === "request") {
    senderResponse(window.localStorage)
  } else {
    try {
      window.localStorage.setItem(key, value)
      // This is required to allow the local storage event listener
      // in the app to see the event
      window.dispatchEvent(new Event("storage"))
      senderResponse("set local storage success:", key, value)
    } catch {
      senderResponse("set local storage error:", key, value)
    }
  }
})
