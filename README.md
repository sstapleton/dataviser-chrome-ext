# Dataviser Client Mock Settings

This is the a google chrome browser extension for the Dataviser project. It works in tandem with the client to create DB items for testing. Its built using React and Webpack.

Run the dataviser client with the common start command. The extension tool will prompt
for a user login if logged out.

```bash
yarn start
```

## Development

Getting started

```bash
yarn install
yarn dev
```

### Add Extension in Google Chrome

When the app is running add the extension in Google Chrome:

1. Go to: chrome://extensions/

2. Select "Load unpacked"

3. Go to the repository and select the build directory. Open from this directory.

4. The extension should now appear in chrome.

5. The application webpage will need to be refreshed once and should now work.
