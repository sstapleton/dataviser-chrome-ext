const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const CopyPlugin = require("copy-webpack-plugin")
const baseManifest = require("./chrome/manifest.json")
const WebpackExtensionManifestPlugin = require("webpack-extension-manifest-plugin")
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin")
const HappyPack = require("happypack")
const happyThreadPool = HappyPack.ThreadPool({ size: 5 })

module.exports = {
  mode: "development",
  devtool: "cheap-module-source-map",
  entry: {
    app: path.resolve(__dirname, "./static/index.tsx")
  },
  output: {
    path: path.resolve(__dirname, "./build"),
    filename: "[name].bundle.js"
  },
  resolve: {
    extensions: ["*", ".js", ".jsx", ".ts", ".tsx"],
    alias: {
      "@": path.resolve(__dirname, "./app")
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "DATAVISER MOCK SETTINGS",
      meta: {
        charset: "utf-8",
        viewport: "width=device-width, initial-scale=1, shrink-to-fit=no",
        "theme-color": "#000000"
      },
      manifest: "manifest.json",
      filename: "index.html",
      template: "./static/index.html",
      hash: true
    }),
    new CopyPlugin({
      patterns: [
        {
          from: "static/background.js"
        }
      ]
    }),
    new CopyPlugin({
      patterns: [
        {
          from: "static/content.js"
        }
      ]
    }),
    new CopyPlugin({
      patterns: [
        {
          from: "chrome/icons",
          to: "icons"
        }
      ]
    }),
    new WebpackExtensionManifestPlugin({
      config: {
        base: baseManifest
      }
    }),
    new ForkTsCheckerWebpackPlugin({
      typescript: {
        diagnosticOptions: {
          semantic: true,
          syntactic: true
        }
      }
    }),
    new HappyPack({
      id: "js",
      threadPool: happyThreadPool,
      loaders: ["babel-loader"]
    }),
    new HappyPack({
      id: "ts",
      threadPool: happyThreadPool,
      loaders: [
        {
          path: "ts-loader",
          query: { happyPackMode: true }
        }
      ]
    })
  ],
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: "happypack/loader?id=ts"
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: "happypack/loader?id=js"
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"]
      }
    ]
  }
}
