import { Fragment } from "react"
import { useStore } from "@/state/state"
import { ITEM_TYPES } from "@/state/items"
import { UpdateProps } from "@/state/item"

// Components
import {
  Autocomplete,
  Box,
  Divider,
  IconButton,
  TextField,
  Tooltip
} from "@mui/material"
import Count from "@/items/fields/count"
import Icon from "@/util/icon"
import ItemFields from "./itemFields"

// Util
import { MIN_COUNT, MAX_COUNT, MAX_TAG_COUNT } from "@/items/fieldsUtil"

interface ItemProps {
  index: number
  name: string
}

const Item = ({ index, name }: ItemProps) => {
  const store = useStore()
  const { items, itemNames, addItem, deleteItem } = store.itemStore
  const showAddBtn = index === items.length - 1
  const storeItem = items.find(item => item.name === name)
  const selected = ITEM_TYPES?.find(type => type.id === name) || ITEM_TYPES[0]
  const remainingTypes = ITEM_TYPES.filter(type => !itemNames.includes(type.id))
  const handleChange = ({ key, value }: UpdateProps) => {
    storeItem?.update({ key, value })
    if (key === "name" && value === "hierarchy")
      storeItem?.update({ key: "ownershipLinkTypes", value: true })
  }

  const handleCountChange = ({ e, key }: CountChangeProps): void => {
    const maximum = key === "tagCount" ? MAX_TAG_COUNT : MAX_COUNT
    let val = e.target.value
    if (val) {
      const numVal = Number(val)
      if (numVal > maximum) val = `${maximum}`
      if (numVal < MIN_COUNT) val = ""
    }
    storeItem?.update({ key, value: String(val) })
  }

  const handleAddItem = () => {
    const newItem = remainingTypes[0].id
    addItem(newItem)
  }

  const handleDeleteItem = () => {
    if (storeItem) deleteItem(storeItem)
  }

  return (
    <Fragment>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        px={4}
        mt={2}
        sx={{
          width: theme => `calc(100% - ${theme.spacing(8)})`
        }}
      >
        <Box display="flex">
          <Autocomplete
            sx={{ width: 300 }}
            disableListWrap
            id={`item-autocomplete`}
            options={[selected, ...remainingTypes]}
            getOptionLabel={option => option.name || ""}
            onChange={(e, item) =>
              handleChange({ key: "name", value: item.id })
            }
            value={selected}
            disableClearable
            renderInput={params => (
              <TextField
                {...params}
                variant="standard"
                label="Item Type"
                fullWidth
              />
            )}
          />
          <Count storeItem={storeItem} handleCountChange={handleCountChange} />
        </Box>
        <Box>
          {showAddBtn && remainingTypes.length > 0 && (
            <Tooltip title="Add Item">
              <IconButton onClick={handleAddItem}>
                <Icon name="add" />
              </IconButton>
            </Tooltip>
          )}
          <Tooltip title="Delete Item">
            <span>
              <IconButton
                onClick={handleDeleteItem}
                disabled={itemNames.length === 1}
              >
                <Icon name="delete" />
              </IconButton>
            </span>
          </Tooltip>
        </Box>
      </Box>
      <ItemFields
        name={name}
        storeItem={storeItem}
        handleCountChange={handleCountChange}
      />
      <Divider />
    </Fragment>
  )
}

export default Item
