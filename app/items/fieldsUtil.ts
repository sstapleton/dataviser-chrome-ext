export const MIN_COUNT = 0
export const MAX_COUNT = 25
export const MAX_TAG_COUNT = 10

export const COMPONENT_MAP = {
  admin: ["user"],
  entities: ["linkChart", "hierarchy", "map", "timeline", "workbook"],
  linkTypes: ["hierarchy", "map"],
  tags: ["workbook"]
}
