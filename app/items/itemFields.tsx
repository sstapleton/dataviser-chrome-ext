import { ItemInstance } from "@/state/item"

// Components
import EntityCount from "@/items/fields/entityCount"
import EntityType from "@/items/fields/entityType"
import IndexType from "@/items/fields/indexType"
import LinkTypes from "@/items/fields/linkTypes"
import Roles from "@/items/fields/roles"
import TagCategoryName from "@/items/fields/tagCategoryName"
import TagCount from "@/items/fields/tagCount"
import { Box } from "@mui/material"

// Util
import { COMPONENT_MAP } from "@/items/fieldsUtil"

export interface ItemFieldsProps {
  name: string
  storeItem?: ItemInstance
  handleCountChange: HandleCountChange
}

const ItemFields = ({
  name,
  storeItem,
  handleCountChange
}: ItemFieldsProps) => (
  <Box px={0} pb={2}>
    <Box pb={2} display="flex">
      {COMPONENT_MAP.admin.includes(name) && <Roles storeItem={storeItem} />}
      {COMPONENT_MAP.tags.includes(name) && (
        <Box display="flex" alignItems="center">
          <TagCount
            storeItem={storeItem}
            handleCountChange={handleCountChange}
          />
          <TagCategoryName storeItem={storeItem} />
        </Box>
      )}
      {COMPONENT_MAP.linkTypes.includes(name) && (
        <LinkTypes name={name} storeItem={storeItem} />
      )}
    </Box>
    {COMPONENT_MAP.entities.includes(name) && (
      <Box display="flex" alignItems="center" pt={1}>
        <EntityCount
          storeItem={storeItem}
          handleCountChange={handleCountChange}
        />
        <IndexType storeItem={storeItem} />
        <EntityType storeItem={storeItem} />
      </Box>
    )}
  </Box>
)

export default ItemFields
