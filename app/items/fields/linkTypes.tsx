import { ItemInstance } from "@/state/item"

// Components
import { Checkbox, FormControlLabel } from "@mui/material"

// Util
import { get } from "lodash"

interface LinkTypesProps {
  name: string
  storeItem?: ItemInstance
}

const LinkTypes = ({ name, storeItem }: LinkTypesProps) => {
  const hasOwnershipLinkTypes = get(storeItem, "ownershipLinkTypes", false)

  return (
    <FormControlLabel
      sx={{ marginLeft: 3 }}
      aria-label="link types"
      control={
        <Checkbox
          id="link-types"
          color="secondary"
          disabled={name === "hierarchy"}
          checked={hasOwnershipLinkTypes}
          onChange={() => {
            const current = hasOwnershipLinkTypes
            storeItem?.update({ key: "ownershipLinkTypes", value: !current })
          }}
        />
      }
      label="Ownership Link Types"
    />
  )
}

export default LinkTypes
