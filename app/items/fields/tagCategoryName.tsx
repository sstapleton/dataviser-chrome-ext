import { ItemInstance } from "@/state/item"

// Components
import { TextField } from "@mui/material"

// Util
import { get } from "lodash"

interface TagCategoryNameProps {
  storeItem?: ItemInstance
}

const TagCategoryName = ({ storeItem }: TagCategoryNameProps) => (
  <TextField
    sx={{
      width: theme => theme.spacing(38),
      marginTop: theme => theme.spacing(-2.75),
      marginLeft: theme => theme.spacing(3)
    }}
    InputLabelProps={{
      sx: { marginTop: -1.25 }
    }}
    id="category-name"
    aria-label="Tag Category Input"
    label="Tag Category Name"
    variant="standard"
    type="text"
    value={get(storeItem, "tagCategoryName", "")}
    inputProps={{ maxLength: 50 }}
    onChange={e => {
      storeItem?.update({ key: "tagCategoryName", value: e.target.value })
    }}
  />
)

export default TagCategoryName
