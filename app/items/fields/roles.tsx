import { ItemInstance } from "@/state/item"

// Components
import { Checkbox, FormControlLabel } from "@mui/material"

// Util
import { get } from "lodash"

interface RolesProps {
  storeItem?: ItemInstance
}

const Roles = ({ storeItem }: RolesProps) => {
  const isAdmin = get(storeItem, "administrator", false)

  return (
    <FormControlLabel
      sx={{ marginLeft: 3 }}
      aria-label="administrator role"
      control={
        <Checkbox
          id="admin-role"
          color="secondary"
          checked={isAdmin}
          onChange={() => {
            const current = isAdmin
            storeItem?.update({ key: "administrator", value: !current })
          }}
        />
      }
      label="Administrator Role"
    />
  )
}

export default Roles
