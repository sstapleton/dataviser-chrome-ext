import { ItemInstance } from "@/state/item"

// Components
import { FormControl, InputLabel, Select, MenuItem } from "@mui/material"

// Util
import { get } from "lodash"

interface IndexTypeProps {
  storeItem?: ItemInstance
}

const EntityType = ({ storeItem }: IndexTypeProps) => (
  <FormControl
    sx={{
      width: theme => theme.spacing(24),
      marginTop: theme => theme.spacing(-2.75),
      marginLeft: theme => theme.spacing(3)
    }}
  >
    <InputLabel id="entity-type-label" sx={{ marginLeft: -2 }}>
      Entity Type
    </InputLabel>
    <Select
      labelId="entity-type-label"
      variant="standard"
      value={get(storeItem, "entityType", "all")}
      label="Entity Type"
      onChange={e => {
        storeItem?.update({ key: "entityType", value: e.target.value })
      }}
    >
      <MenuItem value={"all"}>All</MenuItem>
      <MenuItem value={"academic"}>Academic</MenuItem>
      <MenuItem value={"contract"}>Contract</MenuItem>
      <MenuItem value={"email"}>Email</MenuItem>
      <MenuItem value={"fund"}>Fund</MenuItem>
      <MenuItem value={"government"}>Government</MenuItem>
      <MenuItem value={"location"}>Location</MenuItem>
      <MenuItem value={"organization"}>Organization</MenuItem>
      <MenuItem value={"person"}>Person</MenuItem>
      <MenuItem value={"phone"}>Phone</MenuItem>
      <MenuItem value={"vessel"}>Vessel</MenuItem>
    </Select>
  </FormControl>
)

export default EntityType
