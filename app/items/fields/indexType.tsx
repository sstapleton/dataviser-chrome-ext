import { ItemInstance } from "@/state/item"

// Components
import { FormControl, InputLabel, Select, MenuItem } from "@mui/material"

// Util
import { get } from "lodash"

interface IndexTypeProps {
  storeItem?: ItemInstance
}

const IndexType = ({ storeItem }: IndexTypeProps) => (
  <FormControl
    sx={{
      width: theme => theme.spacing(24),
      marginTop: theme => theme.spacing(-2.75),
      marginLeft: theme => theme.spacing(3)
    }}
  >
    <InputLabel id="index-label" sx={{ marginLeft: -2 }}>
      Index Type
    </InputLabel>
    <Select
      labelId="index-label"
      variant="standard"
      value={get(storeItem, "index", "entity")}
      label="Index Type"
      onChange={e => {
        storeItem?.update({ key: "index", value: e.target.value })
      }}
    >
      <MenuItem value={"entity"}>Resolved Entity</MenuItem>
      <MenuItem value={"entlet"}>Source Entity</MenuItem>
    </Select>
  </FormControl>
)

export default IndexType
