import { ItemInstance } from "@/state/item"

// Components
import { FormControl, FormHelperText, Input, InputLabel } from "@mui/material"

// Util
import { MIN_COUNT, MAX_COUNT } from "@/items/fieldsUtil"
import { get } from "lodash"

interface EntityCountProps {
  storeItem?: ItemInstance
  handleCountChange: HandleCountChange
}

const EntityCount = ({ storeItem, handleCountChange }: EntityCountProps) => (
  <FormControl sx={{ ml: 4, width: theme => theme.spacing(10) }}>
    <InputLabel sx={{ marginLeft: -1 }} shrink htmlFor="entity-count-input">
      Entities
    </InputLabel>
    <Input
      id="entity-count-input"
      type="number"
      inputProps={{ min: `${MIN_COUNT}`, max: `${MAX_COUNT}` }}
      sx={{ pl: 3 }}
      value={get(storeItem, "entityCount", "")}
      onChange={e => handleCountChange({ e, key: "entityCount" })}
    />
    <FormHelperText id="entity-count-input">{`(${MIN_COUNT} - ${MAX_COUNT})`}</FormHelperText>
  </FormControl>
)

export default EntityCount
