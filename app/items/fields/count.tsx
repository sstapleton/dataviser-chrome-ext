import { ItemInstance } from "@/state/item"

// Components
import { FormControl, FormHelperText, Input, InputLabel } from "@mui/material"

// Util
import { MIN_COUNT, MAX_COUNT } from "@/items/fieldsUtil"

interface CountProps {
  storeItem?: ItemInstance
  handleCountChange: HandleCountChange
}

const Count = ({ storeItem, handleCountChange }: CountProps) => (
  <FormControl
    sx={{
      m: 1,
      width: theme => theme.spacing(10),
      marginTop: 0,
      marginLeft: 3
    }}
  >
    <InputLabel
      sx={{
        marginLeft: -1,
        marginTop: 1
      }}
      shrink
      htmlFor="count-input"
    >
      Count
    </InputLabel>
    <Input
      id="count-input"
      type="number"
      inputProps={{ min: `${MIN_COUNT}`, max: `${MAX_COUNT}` }}
      sx={{ pl: 3 }}
      value={storeItem?.count || ""}
      onChange={e => handleCountChange({ e, key: "count" })}
    />
    <FormHelperText id="count-input">{`(${MIN_COUNT} - ${MAX_COUNT})`}</FormHelperText>
  </FormControl>
)

export default Count
