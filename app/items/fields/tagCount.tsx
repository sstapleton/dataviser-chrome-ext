import { ItemInstance } from "@/state/item"

// Components
import { FormControl, FormHelperText, Input, InputLabel } from "@mui/material"

// Util
import { MIN_COUNT, MAX_TAG_COUNT } from "@/items/fieldsUtil"
import { get } from "lodash"

interface TagCountProps {
  storeItem?: ItemInstance
  handleCountChange: HandleCountChange
}

const TagCount = ({ storeItem, handleCountChange }: TagCountProps) => (
  <FormControl sx={{ ml: 4, width: theme => theme.spacing(10) }}>
    <InputLabel sx={{ marginLeft: -1 }} shrink htmlFor="tag-count-input">
      Tags
    </InputLabel>
    <Input
      id="tag-count-input"
      type="number"
      inputProps={{ min: `${MIN_COUNT}`, max: `${MAX_TAG_COUNT}` }}
      sx={{ pl: 3 }}
      value={get(storeItem, "tagCount", "")}
      onChange={e => handleCountChange({ e, key: "tagCount" })}
    />
    <FormHelperText id="tag-count-input">{`(${MIN_COUNT} - ${MAX_TAG_COUNT})`}</FormHelperText>
  </FormControl>
)

export default TagCount
