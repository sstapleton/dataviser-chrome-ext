import { StyledEngineProvider, ThemeProvider } from "@mui/material/styles"
import { theme } from "./theme"
import { hot } from "react-hot-loader/root"

import { StoreProvider } from "@/state/state"
import SVGIconSymbols from "@/util/svgIconSymbols"
import Settings from "@/settings"

const App = (): JSX.Element => {
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme()}>
        <StoreProvider>
          <Settings />
        </StoreProvider>
      </ThemeProvider>
      <SVGIconSymbols />
    </StyledEngineProvider>
  )
}

export default hot(App)
