interface CountChangeProps {
  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  key: string
}

type HandleCountChange = (CountChangeProps) => void
