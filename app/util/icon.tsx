import { uncapitalize } from "@/util/util"
import { SvgIcon } from "@mui/material"

interface IconProps {
  name: string
  sx?: object
}

const Icon = ({ name, sx }: IconProps): JSX.Element => {
  return (
    <SvgIcon sx={sx}>
      <use xlinkHref={`#${uncapitalize(name)}`} />
    </SvgIcon>
  )
}

export default Icon
