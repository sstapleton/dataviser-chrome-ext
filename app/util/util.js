export const uncapitalize = subject => {
  return subject
    ? subject.replace(/^\w/, firstLetter => firstLetter.toLowerCase())
    : ""
}
