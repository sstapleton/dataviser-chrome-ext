import { types } from "mobx-state-tree"

export const tagFieldsInitialState = {
  tagCount: "0",
  tagCategoryName: `Test Category ${Date.now()}`
}

const TagFields = types.model({
  tagCount: types.maybeNull(types.string),
  tagCategoryName: types.string
})

export default TagFields
