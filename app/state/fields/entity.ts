import { types } from "mobx-state-tree"

export const entityFieldsInitialState = {
  entityCount: "0",
  index: "entity",
  entityType: "all"
}

const EntityFields = types.model({
  entityCount: types.maybeNull(types.string),
  index: types.string,
  entityType: types.maybeNull(types.string)
})

export default EntityFields
