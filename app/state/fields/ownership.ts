import { types } from "mobx-state-tree"

const OwnershipFields = types.model({
  ownershipLinkTypes: types.maybeNull(types.boolean)
})

export default OwnershipFields
