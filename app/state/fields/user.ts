import { types } from "mobx-state-tree"

export const userFieldsInitialState = {
  administrator: false
}

const UserFields = types.model({
  administrator: types.boolean
})

export default UserFields
