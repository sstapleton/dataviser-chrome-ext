import { types, Instance } from "mobx-state-tree"
import { v4 as uuidv4 } from "uuid"

import EntityFields from "@/state/fields/entity"
import OwnershipFields from "@/state/fields/ownership"
import TagFields from "@/state/fields/tag"
import UserFields from "@/state/fields/user"

export interface UpdateProps {
  key: string
  value: string | boolean
}

export const baseFieldsInitialState = {
  id: uuidv4(),
  count: "0"
}

export const BaseFields = types
  .model({
    id: types.identifier,
    name: types.string,
    count: types.string
  })
  .actions(self => {
    const update = ({ key, value }: UpdateProps): void => {
      self[key] = value
    }

    return { update }
  })

export const User = types.compose(BaseFields, UserFields).named("User")

export const Workbook = types
  .compose(BaseFields, EntityFields, TagFields)
  .named("Workbook")

export const Hierarchy = types
  .compose(BaseFields, OwnershipFields, EntityFields)
  .named("Hierarchy")

export const LinkChart = types
  .compose(BaseFields, EntityFields)
  .named("LinkChart")

export const Map = types
  .compose(BaseFields, OwnershipFields, EntityFields)
  .named("Map")

export const Timeline = types
  .compose(BaseFields, EntityFields)
  .named("Timeline")

export const Item = types.union(
  User,
  Workbook,
  Hierarchy,
  LinkChart,
  Map,
  Timeline,
  BaseFields
)

export type ItemInstance = Instance<typeof Item>

export default Item
