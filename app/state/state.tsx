import { useContext, createContext, ReactNode } from "react"

// State
import { types, Instance } from "mobx-state-tree"
import Items from "./items"

interface ProviderProps {
  children: ReactNode
}

const RootStore = types.model({
  itemStore: Items
})

// Init RootStore
const initial = {
  itemStore: {}
}
const intialRootStore = RootStore.create(initial)

export type RootInstance = Instance<typeof RootStore>
export const StoreContext = createContext<RootInstance>(intialRootStore)

export const StoreProvider = ({ children }: ProviderProps): JSX.Element => (
  <StoreContext.Provider value={intialRootStore}>
    {children}
  </StoreContext.Provider>
)

export const useStore = () => {
  const store = useContext(StoreContext)

  if (!store) {
    // this is especially useful in TypeScript so you don't need to be checking for null all the time
    throw new Error("Store cannot be null, please add a context provider.")
  }
  return store
}
