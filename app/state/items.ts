import { types, destroy } from "mobx-state-tree"
import {
  Item,
  ItemInstance,
  BaseFields,
  baseFieldsInitialState,
  User,
  Workbook,
  Hierarchy,
  LinkChart,
  Map,
  Timeline
} from "@/state/item"
import { entityFieldsInitialState } from "@/state/fields/entity"
import { tagFieldsInitialState } from "@/state/fields/tag"
import { userFieldsInitialState } from "@/state/fields/user"

// Util
import { get } from "lodash"

export const ITEM_TYPES = [
  {
    name: "User",
    id: "user",
    model: User,
    initialState: {
      ...userFieldsInitialState
    }
  },
  {
    name: "Workbook",
    id: "workbook",
    model: Workbook,
    initialState: {
      ...tagFieldsInitialState,
      ...entityFieldsInitialState
    }
  },
  {
    name: "Hierarchy",
    id: "hierarchy",
    model: Hierarchy,
    initialState: {
      ownershipLinkTypes: true,
      ...entityFieldsInitialState
    }
  },
  {
    name: "Link Chart",
    id: "linkChart",
    model: LinkChart,
    initialState: {
      ...entityFieldsInitialState
    }
  },
  {
    name: "Map",
    id: "map",
    model: Map,
    initialState: {
      ownershipLinkTypes: false,
      ...entityFieldsInitialState
    }
  },
  {
    name: "Timeline",
    id: "timeline",
    model: Timeline,
    initialState: {
      ...entityFieldsInitialState
    }
  }
]

export const Items = types
  .model({
    items: types.optional(types.array(Item), [])
  })
  .views(self => ({
    get hasData(): boolean {
      return self.items.some(item => Number(item.count) > 0)
    },
    get itemNames(): string[] {
      return self.items.map(item => item.name)
    },
    get normalizedData(): string {
      const itemObject = {}
      self.items.forEach(item => {
        if (item.count) {
          const data = {
            count: get(item, "count", 0),
            administrator: get(item, "administrator"),
            ownershipLinkTypes: get(item, "ownershipLinkTypes"),
            tagCount: get(item, "tagCount"),
            tagCategoryName: get(item, "tagCategoryName"),
            entityCount: get(item, "entityCount"),
            index: get(item, "index"),
            entityType: get(item, "entityType")
          }
          itemObject[item.name] = data
        }
      })
      return JSON.stringify(itemObject)
    }
  }))
  .actions(self => {
    const addItem = (name = ""): ItemInstance => {
      const item = ITEM_TYPES?.find(type => type.id === name) || ITEM_TYPES[0]
      const { model = BaseFields } = item

      const newItem = model.create({
        ...baseFieldsInitialState,
        ...item.initialState,
        name: name
      })
      self.items.push(newItem)
      return newItem
    }
    const deleteItem = (item: ItemInstance): void => {
      if (self.items.length > 1) {
        destroy(item)
      }
    }
    const clearItems = (): void => {
      self.items.forEach(item => destroy(item))
    }

    return { addItem, deleteItem, clearItems }
  })

export default Items
