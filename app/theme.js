import { createTheme } from "@mui/material/styles"

export const theme = (props = {}) =>
  createTheme({
    palette: {
      primary: {
        light: "#93caf3",
        main: "#1766b1",
        dark: "#094a92",
        contrastText: "#fff"
      },
      secondary: {
        light: "#fde1b8",
        main: "#f99b32",
        dark: "#ed822b",
        contrastText: "#fff"
      },
      error: {
        main: "#da5654"
      },
      success: {
        main: "#4caf50"
      },
      warning: {
        main: "#ff9800"
      },
      link: {
        main: "#07a"
      },
      flag: {
        main: "#f00"
      }
    }
  })

export default theme()
