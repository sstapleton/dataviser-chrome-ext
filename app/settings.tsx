import { useState, useEffect } from "react"
import { useStore } from "@/state/state"
import { observer } from "mobx-react-lite"

// Components
import Item from "@/items/item"
import { Box, Button, Divider, Typography } from "@mui/material"

// Util
import { isEqual } from "lodash"

const Settings = observer(() => {
  const store = useStore()
  const { items, hasData, normalizedData } = store.itemStore
  const [localStorageData, setLocalStorageData] = useState({})
  const appName = "Dataviser"
  const loginAgreement = "dv_user_agree"
  const userLoggedIn = localStorageData?.[loginAgreement] || false

  // get local storage data if it exists
  useEffect(() => {
    const port = chrome.runtime.connect({ name: "settingsRequest" })
    port.postMessage({ request: "getData" })
    port.onMessage.addListener(function(message) {
      const { response } = message
      if (!isEqual(localStorageData, response)) {
        setLocalStorageData(response)
      }
    })
  }, [])

  useEffect(() => {
    if (items.length === 0) {
      store.itemStore.addItem("user")
    }
  }, [items.length])

  const handleSubmit = (): void => {
    // post updated data
    const port = chrome.runtime.connect({ name: "settingsPost" })
    port.postMessage({ mockData: normalizedData })
    port.onMessage.addListener(function(message) {
      console.log("POST Response:", message)
    })
  }

  const handleClear = (): void => {
    store.itemStore.clearItems()
  }

  return (
    <Box
      sx={{
        height: "400px",
        width: "600px",
        margin: theme => theme.spacing(-1),
        padding: 0
      }}
    >
      <Typography
        variant="h6"
        sx={{
          padding: theme => theme.spacing(2, 2),
          color: "white",
          backgroundColor: theme => theme.palette.primary.main
        }}
      >
        {appName} Mock Data Tool
      </Typography>
      <Divider />
      {!userLoggedIn ? (
        <Box
          sx={{
            height: theme => `calc(100% - ${theme.spacing(8)})`,
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Typography variant="h6">Log in to Dataviser to use tool.</Typography>
        </Box>
      ) : (
        <Box
          sx={{
            height: theme => `calc(100% - ${theme.spacing(8)})`,
            width: "100%",
            overflow: "auto",
            paddingBottom: theme => theme.spacing(2)
          }}
        >
          {items.map((item, i) => (
            <Item key={i} index={i} name={item.name} />
          ))}
        </Box>
      )}
      <Divider />
      <Box py={1} pr={2} display="flex" justifyContent="flex-end">
        <Button
          variant="contained"
          onClick={handleClear}
          sx={{ marginRight: 2 }}
          color="secondary"
        >
          Clear
        </Button>
        <Button disabled={!hasData} variant="contained" onClick={handleSubmit}>
          Submit
        </Button>
      </Box>
    </Box>
  )
})

export default Settings
